package com.charity.service.impl;

import com.charity.domain.User;
import com.charity.exception.BusinessException;
import com.charity.mapper.UserMapper;
import com.charity.model.bo.AccountType;
import com.charity.model.request.DetailUser;
import com.charity.repository.UserRepository;
import com.charity.service.UserInternalService;
import com.charity.util.ErrorCode;
import com.charity.util.ErrorMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserInternalServiceImpl implements UserInternalService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    @Override
    public DetailUser getUserByLogin(String login) {
        User user = userRepository.findByLogin(login);
        if (user == null)
            throw new BusinessException(ErrorCode.USER_NOT_FOUND, ErrorMessage.USER_NOT_FOUND);
        return userMapper.mapToUserDetail(user);
    }
}
