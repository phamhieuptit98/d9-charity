package com.charity.service.impl;

import com.charity.exception.BusinessException;
import com.charity.repository.ExtraRepository;
import com.charity.service.SequenceInternalService;
import com.charity.util.ErrorCode;
import com.charity.util.Global;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SequenceInternalServiceImpl implements SequenceInternalService {

    private final ExtraRepository extraRepository;

    /**
     * generateEmployeeCode
     *
     * @return String
     */
    @Override
    public String generateEmployeeCode() {

        Long nextNoOrder = extraRepository.getNextValueSeq(Global.EMPLOYEE_CODE_NO_SEQUENCE);

        if (nextNoOrder == null)
            throw new BusinessException(ErrorCode.COULD_NOT_GENERATE_CODE, "Could not Generate code for object");

        String code = "SNV";
        String number = String.format("%04d", nextNoOrder);

        return code + number;
    }


}
