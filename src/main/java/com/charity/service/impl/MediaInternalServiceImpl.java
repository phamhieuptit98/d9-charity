package com.charity.service.impl;

import com.charity.domain.Media;
import com.charity.exception.BusinessException;
import com.charity.repository.MediaRepository;
import com.charity.service.MediaInternalService;
import com.charity.util.ErrorCode;
import com.charity.util.ErrorMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MediaInternalServiceImpl implements MediaInternalService {

    private final MediaRepository repository;

    /**
     * get media
     *
     * @param id id
     * @return Media
     */
    @Override
    public Media findById(int id) {
        return repository.findById(id).orElseThrow(() -> new BusinessException(ErrorCode.FILE_NOT_EXIST, ErrorMessage.FILE_NOT_EXIST));
    }

    /**
     * save all media
     *
     * @param medias medias
     * @return List<Media>
     */
    @Override
    public List<Media> saveAll(List<Media> medias) {
        return repository.saveAll(medias);
    }

}
