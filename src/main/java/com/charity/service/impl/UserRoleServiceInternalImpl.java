package com.charity.service.impl;

import com.charity.domain.UserRole;
import com.charity.repository.UserRoleRepository;
import com.charity.service.UserRoleServiceInternal;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class UserRoleServiceInternalImpl implements UserRoleServiceInternal {

    private final UserRoleRepository userRoleRepository;

    @Override
    public List<UserRole> findByUserID(String userId) {
        return userRoleRepository.findByUserId(userId);
    }
}
