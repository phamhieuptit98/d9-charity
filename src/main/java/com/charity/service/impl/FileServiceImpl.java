package com.charity.service.impl;

import com.charity.domain.Media;
import com.charity.exception.BusinessException;
import com.charity.mapper.FileMapper;
import com.charity.model.bo.TypeMedia;
import com.charity.model.dto.FileResponse;
import com.charity.model.request.FileRequest;
import com.charity.repository.MediaRepository;
import com.charity.service.FileService;
import com.charity.service.MediaInternalService;
import com.charity.util.Constant;
import com.charity.util.ErrorCode;
import com.charity.util.ErrorMessage;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final Logger logger = LogManager.getLogger(getClass());
    private final FileMapper mapper;
    private final MediaRepository repository;
    private final MediaInternalService mediaInternalService;
    private final ServletContext servletContext;
    @Value("${app.server.upload.folder.path}")
    private String uploadDir;
    @Value("${app.server.download.folder.path}")
    private String downloadDir;

    @Override
    public void download(Integer fileId, HttpServletResponse response) {
//        String file = "/home/hieupv/Desktop/uploads/2022-03-16/35300100-2c87-4d40-b001-002c87ad40b0-json.json";

//        String a = "/home/hieupv/Videos/uploads/2022-03-16/images/2022-03-16/35300100-2c87-4d40-b001-002c87ad40b0-json.json"
        Media media = mediaInternalService.findById(fileId);

        String[] splitFile = media.getUrl().split("/");
        String fileName = splitFile[splitFile.length - 1];

        MediaType mediaType = getMediaTypeForFileName(this.servletContext, fileName);
        File file = new File(downloadDir + File.separator + fileName);

        response.setContentType(mediaType.getType());
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName());
        response.setContentLength((int) file.length());

        try {
            BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(file));
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
        String mineType = servletContext.getMimeType(fileName);
        try {
            return MediaType.parseMediaType(mineType);
        } catch (Exception e) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }

    /**
     * upload file
     *
     * @param
     * @return
     */
    @Override
    public List<FileResponse> upload(MultipartFile[] files, TypeMedia type) {
        if (files.length > 10)
            throw new BusinessException(ErrorCode.ONLY_UPLOAD_10_FILES, ErrorMessage.ONLY_UPLOAD_10_FILES);

        List<FileRequest> fileRequests = new ArrayList<>();

        Arrays.stream(files).forEach(file -> {
            String fileName = getFileName(file);
            try {
                saveFile(uploadDir, fileName, file);
                fileRequests.add(new FileRequest().setOwner(null)
                        .setUrl(createImageSource(fileName).toString())
                        .setTypeMedia(type)
                        .setName(file.getOriginalFilename()));
            } catch (IOException e) {
                logger.error(FileService.class, e);
            }
        });

        List<Media> mediaList = mapper.mapFileEntity(fileRequests);
        repository.insertAll(mediaList);

        return mapper.mapToResponse(mediaList);
    }

    /**
     * @param fileName
     * @return
     */
    private StringBuilder createImageSource(String fileName) {
        return new StringBuilder("/images/").append(LocalDate.now()).append("/")
                .append(fileName);
    }

    /**
     * @param uploadDir
     * @param fileName
     * @param multipartFile
     * @throws IOException
     */
    private void saveFile(String uploadDir, String fileName,
                          MultipartFile multipartFile) throws IOException {
        LocalDate folderName = LocalDate.now();
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        Path uploadPathFolder = Paths.get(uploadDir + "/" + folderName);

        if (!Files.exists(uploadPathFolder)) {
            Files.createDirectories(uploadPathFolder);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPathFolder.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not save image file: " + fileName, e);
        }
    }


    private String getFileName(MultipartFile file) {
        return modifyFileName(Objects.requireNonNull(file.getOriginalFilename()));
    }

    /**
     * @param fileName
     * @return
     */
    private String modifyFileName(String fileName) {
        return Constant.generateUUID() + Constant.SEPARATOR + fileName.replaceAll("\\s+", " ").replace(' ', '-');
    }


}
