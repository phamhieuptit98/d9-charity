package com.charity.service.impl;

import com.charity.config.Constants;
import com.charity.domain.User;
import com.charity.domain.UserRole;
import com.charity.exception.BusinessException;
import com.charity.mapper.UserMapper;
import com.charity.model.dto.AdminUserDTO;
import com.charity.model.dto.UserDTO;
import com.charity.model.dto.UserResponse;
import com.charity.model.request.ManagedUserVM;
import com.charity.repository.RoleRepository;
import com.charity.repository.UserRepository;
import com.charity.repository.UserRoleRepository;
import com.charity.security.SecurityUtils;
import com.charity.service.UserService;
import com.charity.util.ErrorCode;
import com.charity.util.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.security.RandomUtil;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    private final CacheManager cacheManager;

    private final MailService mailService;

    private final UserMapper userMapper;

    private final UserRoleRepository userRoleRepository;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, CacheManager cacheManager, MailService mailService, UserMapper userMapper, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.cacheManager = cacheManager;
        this.mailService = mailService;
        this.userMapper = userMapper;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public List<UserResponse> getUserDonate() {

        List<User> userList = userRepository.findByActivated(true);
        Set<String> userIds = userList.stream().map(User::getId).collect(Collectors.toSet());


        return null;
    }

    public UserResponse newUser(AdminUserDTO userDTO) {

        User newUser;
        if (userDTO.getId() != null) {
            throw new BusinessException(ErrorCode.USER_ALREADY, ErrorMessage.USER_ALREADY);
            // Lowercase the user login before comparing with database
        } else if (userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()) {
            throw new BusinessException(ErrorCode.EMAIL_IS_ALREADY_IN_USE, ErrorMessage.EMAIL_IS_ALREADY);
        } else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
            throw new BusinessException(ErrorCode.EMAIL_IS_ALREADY_IN_USE, ErrorMessage.EMAIL_IS_ALREADY);
        } else {
            newUser = createUser(userDTO);
            mailService.sendCreationEmail(newUser);
        }
        return userMapper.mapToResponse(newUser);
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key).map(user -> {
            // activate given user for the registration key.
            user.setActivated(true);
            user.setActivationKey(null);
            this.clearUserCaches(user);
            log.debug("Activated user: {}", user);
            return user;
        });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key).filter(user -> user.getResetDate().isAfter(Instant.now().minus(1, ChronoUnit.DAYS))).map(user -> {
            user.setPassword(passwordEncoder.encode(newPassword));
            user.setResetKey(null);
            user.setResetDate(null);
            this.clearUserCaches(user);
            return user;
        });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail).filter(User::isActivated).map(user -> {
            user.setResetKey(RandomUtil.generateResetKey());
            user.setResetDate(Instant.now());
            this.clearUserCaches(user);
            return user;
        });
    }

    @Transactional(rollbackFor = Exception.class)
    public User registerUser(ManagedUserVM userDTO, String password) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new UsernameAlreadyUsedException();
            }
        });
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        newUser = userMapper.mapToEntity(newUser, userDTO);
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setPassword(encryptedPassword);
        userRepository.insert(newUser);
        if (userDTO.getAuthorities() != null) {
            String userId = newUser.getId();
            List<UserRole> userRoleList = userDTO.getAuthorities().stream().map(it -> mapToEntityUserRole(it, userId)).collect(Collectors.toList());
            userRoleRepository.insertAll(userRoleList);
        }
        this.clearUserCaches(newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.isActivated()) {
            return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(AdminUserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setName(userDTO.getName());

        if (userDTO.getAddress() != null) user.setAddress(userDTO.getAddress());
        if (userDTO.getAge() != null) user.setAge(userDTO.getAge());
        if (userDTO.getTotalMoney() != null) user.setTotalMoney(userDTO.getTotalMoney());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        if (userDTO.getIdentity() != null) user.setIdentity(userDTO.getIdentity());

        if (userDTO.getPhone() != null) user.setPhone(userDTO.getPhone());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        userRepository.insert(user);
        if (userDTO.getAuthorities() != null) {
           List<UserRole> userRoleList = userDTO.getAuthorities().stream().map(it -> mapToEntityUserRole(it, user.getId())).collect(Collectors.toList());
           userRoleRepository.insertAll(userRoleList);
        }
        this.clearUserCaches(user);
        return user;
    }

        private UserRole mapToEntityUserRole(String role, String userId){
        return new UserRole().setUserId(userId).setRoleName(role);
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param name     first name of user.
     * @param email    email id of user.
     * @param langKey  language key.
     * @param imageUrl image URL of user.
     */
    public void updateUser(String name, Integer age, BigDecimal totalMoney, String identity, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
            user.setName(name);
            user.setAge(age);
            user.setTotalMoney(totalMoney);
            user.setIdentity(identity);
            if (email != null) {
                user.setEmail(email.toLowerCase());
            }
            user.setLangKey(langKey);
            user.setImageUrl(imageUrl);
            this.clearUserCaches(user);
            log.debug("Changed Information for User: {}", user);
        });
    }

    @Transactional
    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
            String currentEncryptedPassword = user.getPassword();
            if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                throw new InvalidPasswordException();
            }
            String encryptedPassword = passwordEncoder.encode(newPassword);
            user.setPassword(encryptedPassword);
            this.clearUserCaches(user);
            log.debug("Changed password for User: {}", user);
        });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllPublicUsers(Pageable pageable) {
        return userRepository.findAllByIdNotNullAndActivatedIsTrue(pageable).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }
//
//    /**
//     * Not activated users should be automatically deleted after 3 days.
//     * <p>
//     * This is scheduled to get fired everyday, at 01:00 (am).
//     */
//    @Scheduled(cron = "0 0 1 * * ?")
//    public void removeNotActivatedUsers() {
//        userRepository.findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS)).forEach(user -> {
//            log.debug("Deleting not activated user {}", user.getLogin());
//            userRepository.delete(user);
//            this.clearUserCaches(user);
//        });
//    }

//    /**
//     * Gets a list of all the authorities.
//     *
//     * @return a list of all the authorities.
//     */
//    @Transactional(readOnly = true)
//    public List<String> getAuthorities() {
//        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
//    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }
}
