package com.charity.service;

public interface SequenceInternalService {

    /**
     * Generate Employee Code
     *
     * @return
     */
    String generateEmployeeCode();
}
