package com.charity.service;

import com.charity.model.bo.TypeMedia;
import com.charity.model.dto.FileResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.List;

public interface FileService {

    void download(Integer fileId, HttpServletResponse response) throws FileNotFoundException;

    List<FileResponse> upload(MultipartFile[] files, TypeMedia type);
}
