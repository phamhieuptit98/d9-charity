package com.charity.service;

import com.charity.domain.User;
import com.charity.model.dto.AdminUserDTO;
import com.charity.model.dto.UserDTO;
import com.charity.model.dto.UserResponse;
import com.charity.model.request.ManagedUserVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<UserResponse> getUserDonate();

    UserResponse newUser(AdminUserDTO userDTO);

    Optional<User> activateRegistration(String key);

    Optional<User> completePasswordReset(String newPassword, String key);

    Optional<User> requestPasswordReset(String mail);

    User registerUser(ManagedUserVM userDTO, String password);

    User createUser(AdminUserDTO userDTO);

    void updateUser(String name, Integer age, BigDecimal totalMoney, String identity, String email, String langKey, String imageUrl);

    void changePassword(String currentClearTextPassword, String newPassword);

    Page<UserDTO> getAllPublicUsers(Pageable pageable);

    Optional<User> getUserWithAuthoritiesByLogin(String login);

    Optional<User> getUserWithAuthorities();

//    void removeNotActivatedUsers();

//    List<String> getAuthorities();

}
