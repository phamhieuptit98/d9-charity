package com.charity.service;

import com.charity.model.request.DetailUser;

public interface UserInternalService {

    DetailUser getUserByLogin(String login);
}
