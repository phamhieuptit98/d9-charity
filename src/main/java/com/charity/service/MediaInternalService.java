package com.charity.service;

import com.charity.domain.Media;

import java.util.List;

public interface MediaInternalService {

    List<Media> saveAll(List<Media> medias);

    Media findById(int id);
}
