package com.charity.service;

import com.charity.domain.UserRole;

import java.util.List;

public interface UserRoleServiceInternal {

    List<UserRole> findByUserID(String userId);
}
