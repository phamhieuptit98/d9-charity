package com.charity.model.bo;

public enum StatusCommon {
    ACTIVE, INACTIVE, DELETE
}
