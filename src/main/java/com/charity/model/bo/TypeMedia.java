package com.charity.model.bo;

public enum TypeMedia {
    IMAGE, EXCEL, OTHER, WORD
}
