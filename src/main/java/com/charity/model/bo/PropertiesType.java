package com.charity.model.bo;

public enum PropertiesType {
    PAYMENT_METHOD_CONTRACT, TYPE_CONTRACT, TRANSPORT_METHOD, POSITIONS, DEPARTMENT
}
