package com.charity.model.bo;

public enum CategoryStatus {

    ACTIVE, INACTIVE, DELETE
}
