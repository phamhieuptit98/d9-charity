package com.charity.model.bo;

public enum AccountType {
    ADMIN, USER, SUPPORTER
}
