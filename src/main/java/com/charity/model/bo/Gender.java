package com.charity.model.bo;

public enum Gender {
    MALE, FEMALE, THIRD_GENDER
}
