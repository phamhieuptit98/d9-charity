package com.charity.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class UserResponse {

    private String id;

    private String login;

    private String name;

    private String email;

    private Integer age;

    private String address;

    private String identity;

    private String phone;

    private BigDecimal bankAccount;

}
