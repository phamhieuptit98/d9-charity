package com.charity.model.request;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class LoginRequest {

    private String username;

    private String password;

    public String getUsername() {
        return StringUtils.isBlank(username) ? null : username.trim();
    }

    public String getPassword() {
        return StringUtils.isBlank(password) ? null : password.trim();
    }
}
