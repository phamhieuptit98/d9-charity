package com.charity.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class FilterProjectRequest {

    private Integer categoryId;

    private LocalDate startAt;

    private LocalDate endAt;

    private String projectName;

    private String userName;
}
