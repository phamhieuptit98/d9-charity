package com.charity.model.request;

import com.dslplatform.json.CompiledJson;
import com.charity.model.bo.PropertiesCode;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@CompiledJson
public class PropertiesRequest {

    private PropertiesCode code;

}
