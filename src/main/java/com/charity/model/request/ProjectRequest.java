package com.charity.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

@Data
@Accessors(chain = true)
public class ProjectRequest {

    private Integer categoryId;

    private String name;

    private String code;

    private String description;

    private OffsetDateTime startDate;

    private OffsetDateTime endDate;

    private Double moneyEstimate;
}
