package com.charity.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "user_project")
@Table(indexes = {
//        @Index(name="idx_driver_phone", columnList = "phone"),
//        @Index(name="idx_driver_status", columnList = "status"),
})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserProject extends BaseEntity{

    @Id
    @SequenceGenerator(name = "user_project_id_seq", sequenceName = "user_project_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_project_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "int default 0", nullable = false)
    private Integer projectId;
}
