package com.charity.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "roles")
@Data
@Accessors(chain = true)
public class Role extends BaseEntity {

    @NotNull
    @Size(max = 50)
    @Id
    @Column(length = 50)
    private String name;
}
