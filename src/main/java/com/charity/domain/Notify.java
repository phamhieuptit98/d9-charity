package com.charity.domain;

import com.charity.model.bo.StatusCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "notify")
@Table(indexes = {

})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Notify extends BaseEntity{

    @Id
    @SequenceGenerator(name = "notify_id_seq", sequenceName = "notify_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notify_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(200)", nullable = false)
    private String title;

    @Column(columnDefinition = "varchar(500)", nullable = false)
    private String content;

    @Column(columnDefinition = "varchar(20) default 'ACTIVE'", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusCommon status;

    @Column(columnDefinition = "timestamp", nullable = false)
    private LocalDate expiredDate;

    @Column(columnDefinition = "timestamp")
    private LocalDate deleteAt;
}
