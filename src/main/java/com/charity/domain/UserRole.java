package com.charity.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "user_role")
@Table(indexes = {

})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserRole extends BaseEntity{

    @Id
    @SequenceGenerator(name = "user_donate_id_seq", sequenceName = "user_donate_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_donate_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String roleName;
}
