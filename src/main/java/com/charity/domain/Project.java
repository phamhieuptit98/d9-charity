package com.charity.domain;

import com.charity.model.bo.ProjectStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity(name = "project")
@Table(indexes = {

})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Project extends BaseEntity {

    @Id
    @SequenceGenerator(name = "project_id_seq", sequenceName = "project_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(20) default 'WAITING_ACCEPT'", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProjectStatus status;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String name;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "varchar(500)", nullable = false)
    private String description;

    @Column(columnDefinition = "int default 0", nullable = false)
    private Integer categoryId;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal minMoney;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal maxMoney;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal targetValue;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal moneyEstimate;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal moneyActual;

    @Column(columnDefinition = "timestamp", nullable = false)
    private LocalDate endDate;

    @Column(columnDefinition = "timestamp", nullable = false)
    private LocalDate startDate;

    @Column(columnDefinition = "timestamp")
    private LocalDate deleteAt;

    @Column(columnDefinition = "long default 0", nullable = false)
    private Long countFollow;
}
