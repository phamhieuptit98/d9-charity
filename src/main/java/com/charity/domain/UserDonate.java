package com.charity.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity(name = "user_donate")
@Table(indexes = {

})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserDonate extends BaseEntity {

    @Id
    @SequenceGenerator(name = "user_donate_id_seq", sequenceName = "user_donate_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_donate_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "int default 0", nullable = false)
    private Integer projectId;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal value;

    @Column(columnDefinition = "timestamp")
    private LocalDate donateAt;
}
