package com.charity.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "credit_card")
@Table(indexes = {

})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CreditCard extends BaseEntity{

    @Id
    @SequenceGenerator(name = "credit_card_id_seq", sequenceName = "credit_card_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "credit_card_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String cardCompany;

    @Column(columnDefinition = "varchar(50)", nullable = false)
    private String cardType;

    @Column(columnDefinition = "timestamp", nullable = false)
    private LocalDate expiredDate;
}
