package com.charity.domain;

import com.charity.model.bo.StatusCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity(name = "gift")
@Table(indexes = {
//        @Index(name="idx_driver_phone", columnList = "phone"),
//        @Index(name="idx_driver_status", columnList = "status"),
})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Gift extends BaseEntity {

    @Id
    @SequenceGenerator(name = "gift_id_seq", sequenceName = "gift_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gift_id_seq")
    private Integer id;

    @Column(columnDefinition = "int default 0", nullable = false)
    private Integer projectId;

    @Column(columnDefinition = "varchar(200)", nullable = false)
    private String title;

    @Column(columnDefinition = "varchar(500)", nullable = false)
    private String description;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal value;

    @Column(columnDefinition = "timestamp")
    private LocalDate deleteAt;

    @Column(columnDefinition = "decimal(15,2) default '0'", nullable = false)
    private BigDecimal targetMoney;

    @Column(columnDefinition = "varchar(20) default 'ACTIVE'", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusCommon status;
}
