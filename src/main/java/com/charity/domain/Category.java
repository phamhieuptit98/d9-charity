package com.charity.domain;

import com.charity.model.bo.StatusCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "category")
@Table(indexes = {
//        @Index(name="idx_driver_phone", columnList = "phone"),
//        @Index(name="idx_driver_status", columnList = "status"),
})
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Category extends BaseEntity {

    @Id
    @SequenceGenerator(name = "category_id_seq", sequenceName = "category_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_id_seq")
    private Integer id;

    @Column(columnDefinition = "varchar(300)")
    private String name;

    @Column(columnDefinition = "varchar(300)")
    private String description;

    @Column(columnDefinition = "varchar(20) default 'ACTIVE'", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusCommon status;

    @Column(columnDefinition = "int default 0", nullable = false)
    private Integer type;


}
