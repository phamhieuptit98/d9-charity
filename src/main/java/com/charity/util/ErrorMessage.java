package com.charity.util;

public interface ErrorMessage {

    String MESSAGE_ERROR = "Invalid field format";

    String REQUIRE = "require";

    String TYPE = "type";

    String INVALID = "Invalid";

    String EMAIL_IS_ALREADY = "Email is already in use!, userManagement, email exists";

    String USER_ALREADY = "A new user cannot already have an ID, userManagement, id exists";

    String ONLY_UPLOAD_10_FILES = "Only 10 files can be inserted once";

    String INVALID_PASSWORD_TYPE = "Incorrect password";

    String FILE_NOT_EXIST = "File not exist";

    String USER_NOT_FOUND = "User not found!";
}
