package com.charity.repository;

import com.charity.domain.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> ,InsertUpdateRepository<UserRole>{

    List<UserRole> findByUserId(String userId);
}
