package com.charity.controller;

import com.charity.model.bo.TypeMedia;
import com.charity.model.dto.FileResponse;
import com.charity.model.dto.Response;
import com.charity.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/files")
@Validated
@RequiredArgsConstructor
public class FileController {

    private final FileService service;

    @PostMapping("/upload")
    public Response<List<FileResponse>> upload(@RequestParam("files") MultipartFile[] files, @NotNull @RequestParam("type") TypeMedia type) {
        return Response.ofSucceeded(service.upload(files, type));
    }

    @GetMapping("/downloads")
    public void download(@RequestParam("file-id") Integer fileId,
                         HttpServletResponse response) throws FileNotFoundException {
        service.download(fileId, response);
    }
}
